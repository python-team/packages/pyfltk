/* File : Fl_Radio_Button.i */
//%module Fl_Radio_Button

%{
#include "FL/Fl_Radio_Button.H"
%}

%include "macros.i"

CHANGE_OWNERSHIP(Fl_Radio_Button)

%include "FL/Fl_Radio_Button.H"
