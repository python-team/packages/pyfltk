/* File : Fl_FormsBitmap.i */
//%module Fl_FormsBitmap

%{
#include "FL/Fl_FormsBitmap.H"
%}

%include "macros.i"

CHANGE_OWNERSHIP(Fl_FormsBitmap)

%include "FL/Fl_FormsBitmap.H"
