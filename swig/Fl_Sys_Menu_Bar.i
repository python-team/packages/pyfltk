/* File : Fl_Sys_Menu_Bar.i */
//%module Fl_Sys_Menu_Bar

%{
#include "FL/Fl_Sys_Menu_Bar.H"
%}

%include "macros.i"

CHANGE_OWNERSHIP(Fl_Sys_Menu_Bar)

%include "FL/Fl_Sys_Menu_Bar.H"

