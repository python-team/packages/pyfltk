/* File : Fl_Simple_Counter.i */
//%module Fl_Simple_Counter

%{
#include "FL/Fl_Simple_Counter.H"
%}

%include "macros.i"

CHANGE_OWNERSHIP(Fl_Simple_Counter)

%include "FL/Fl_Simple_Counter.H"
