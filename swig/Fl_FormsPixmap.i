/* File : Fl_FormsPixmap.i */
//%module Fl_FormsPixmap

%{
#include "FL/Fl_FormsPixmap.H"
%}

%include "macros.i"

CHANGE_OWNERSHIP(Fl_FormsPixmap)

%include "FL/Fl_FormsPixmap.H"
