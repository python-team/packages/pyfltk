/* File : Fl_Radio_Light_Button.i */
//%module Fl_Radio_Light_Button

%{
#include "FL/Fl_Radio_Light_Button.H"
%}

%include "macros.i"

CHANGE_OWNERSHIP(Fl_Radio_Light_Button)

%include "FL/Fl_Radio_Light_Button.H"
