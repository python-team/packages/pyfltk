/* File : Fl_Radio_Round_Button.i */
//%module Fl_Radio_Round_Button

%{
#include "FL/Fl_Radio_Round_Button.H"
%}

%include "macros.i"

CHANGE_OWNERSHIP(Fl_Radio_Round_Button)

%include "FL/Fl_Radio_Round_Button.H"
