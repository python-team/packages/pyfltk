#!/usr/bin/env python
# -*- coding: utf-8 -*-

"Python wrapper for the fltk GUI toolkit"

__license__ = "LGPL 2.0"
__version__ = "1.3.9"

from .fltk import *
